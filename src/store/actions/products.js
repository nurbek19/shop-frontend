import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {CREATE_PRODUCT_SUCCESS, FETCH_PRODUCT_SUCCESS} from "./actionTypes";


export const fetchProductSuccess = products => {
    return {type: FETCH_PRODUCT_SUCCESS, products}
};

export const fetchProducts = () => {
  return dispatch => {
      return axios.get('/products').then(response => {
          dispatch(fetchProductSuccess(response.data));
      })
  }
};

export const createProductSuccess = () => {
  return {type: CREATE_PRODUCT_SUCCESS}
};

export const createProduct = productData => {
  return dispatch => {

      return axios.post('/products', productData).then(() => {
          dispatch(createProductSuccess());
          dispatch(push('/'));
      })
  }
};